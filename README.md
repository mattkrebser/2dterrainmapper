# README #

2D Top-Down Terrain Generator

### What is this repository for? ###

* Generate practically any 2D-top-down environment automatically.
* Input Tile textures and other sprite objects.
* Then set terrain randomization parameters
* Then a fully randomized world is generated.

### How do I get set up? ###

* This repository is a unity project folder.
* Download the repository and open it in Unity.
* For generation, open the 'play scene' if it isn't open already
* Make sure to un-tick the '2d' tab in the scene view
* The play scene has an example, so press the play button to see what happens
* Observe the example object to understand the input format.
* There are many tool tips to help

![Screenshot.png](https://bitbucket.org/repo/6kGpxe/images/1809157302-Screenshot.png)