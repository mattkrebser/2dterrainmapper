﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DrawTerrain
{
    /// <summary>
    /// All terrain materials
    /// </summary>
    private static Dictionary<int, Material> materials = new Dictionary<int, Material>();

    private static Dictionary<Texture2D, Material> terrain_materials = new Dictionary<Texture2D, Material>();

    private static Material Default;

    /// <summary>
    /// Draw the input region. The existing region gameobjects will be destroyed
    /// </summary>
    /// <param name="draw_region"></param>
    public static void Draw(Region draw_region, TerrainTypes terrain)
    {
        materials.Clear();
        terrain_materials.Clear();

        //fetch the world parent
        GameObject world = GameObject.FindWithTag("TerrainParent");

        //destroy any children (existing terrains)
        foreach (Transform t in world.GetComponentsInChildren<Transform>(true))
        {
            if (t.parent == world.transform)
            {
                MonoBehaviour.Destroy(t.gameObject);
            }
        }

        //iterate through each chunk
        for (int i = 0; i < draw_region.world_size; i++)
        {
            for (int j = 0; j < draw_region.world_size; j++)
            {
                MakeChunk(i, j, draw_region, world.transform, terrain);
            }
        }
    }

    /// <summary>
    /// Make an individual chunk
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="r"></param>
    /// <param name="p"></param>
    static void MakeChunk(int i, int j, Region r, Transform p, TerrainTypes terrain)
    {
        DrawBaseTerrain(i, j, r, p, terrain);
        DrawTerrainObjects(i, j, r, p, terrain);
    }

    /// <summary>
    /// Draw underlying background terrain
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="r"></param>
    /// <param name="p"></param>
    static void DrawBaseTerrain(int i, int j, Region r, Transform p, TerrainTypes terrain)
    {
        //local dictionary for this chunk, used to determine if a material is being used
        Dictionary<int, MeshObject> terrain_objects = new Dictionary<int, MeshObject>();

        //build vertice/uv arrays
        for (int x = 0; x < TerrainConstants.ChunkSize; x++)
        {
            for (int y = 0; y < TerrainConstants.ChunkSize; y++)
            {
                //draw middle
                ProcessCoordinate(i, j, x, y, terrain_objects, r, terrain);

                //do terrain edges
                if (r.chunks[i,j].terrain_padding[x,y] != null)
                {
                    HashSet<terrain_pad_dat> set = r.chunks[i, j].terrain_padding[x, y];
                    foreach (terrain_pad_dat pdat in set)
                        ProcessCoordinate(i, j, x, y, terrain_objects, r, pdat.biome, pdat.position, terrain);
                }
            }
        }

        //build objects
        foreach (MeshObject m in terrain_objects.Values)
        {
            GameObject newObj = new GameObject(i.ToString() + ", " + j.ToString());
            MeshRenderer mr = newObj.AddComponent<MeshRenderer>();
            mr.sortingOrder = m.priority;
            MeshFilter mf = newObj.AddComponent<MeshFilter>();
            mr.material = GetMaterial(m.texture_value, terrain);
            mf.mesh.vertices = m.vertices.ToArray();
            mf.mesh.uv = m.uv_map.ToArray();
            mf.mesh.triangles = m.triangles.ToArray();
            mf.mesh.Optimize();
            newObj.transform.SetParent(p);
        }
    }

    /// <summary>
    /// Builds vertice/uv arrays
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="objects"></param>
    /// <param name="r"></param>
    static void ProcessCoordinate(int i, int j, int x, int y, Dictionary<int, MeshObject> objects, Region r, TerrainTypes terrain)
    {
        //get values
        ushort value = r.chunks[i, j].terrain[x, y];

        //compute chunk position
        int chunkx = i * TerrainConstants.ChunkSize;
        int chunky = j * TerrainConstants.ChunkSize;

        //Get mesh Object
        MeshObject mObj = default(MeshObject);
        if (!objects.TryGetValue(value, out mObj))
        {
            mObj.vertices = new List<Vector3>();
            mObj.uv_map = new List<Vector2>();
            mObj.triangles = new List<int>();
            mObj.texture_value = value;
            mObj.priority = terrain.GetBiomePriority(value);
            objects.Add(value, mObj);
        }

        //reference names
        List<Vector3> v = mObj.vertices; List<Vector2> u = mObj.uv_map;
        List<int> t = mObj.triangles;

        //count before adding vertices
        int start = v.Count;

        //add vertices
        v.Add(new Vector3(chunkx + x, 0, chunky + y));
        v.Add(new Vector3(chunkx + x, 0, chunky + y + 1));
        v.Add(new Vector3(chunkx + x + 1, 0, chunky + y + 1));

        v.Add(new Vector3(chunkx + x, 0, chunky + y));
        v.Add(new Vector3(chunkx + x + 1, 0, chunky + y + 1));
        v.Add(new Vector3(chunkx + x + 1, 0, chunky + y));

        //assign triangles indices
        t.Add(start); t.Add(start + 1); t.Add(start + 2);
        t.Add(start + 3); t.Add(start + 4); t.Add(start + 5);

        //just for reference: 
        /*
            0  1  2  3  4  5  6  7
            8  9  10 11 12 13 14 15

        */
        //all tilesheet layouts are pictured like above. See Resources/TerrainSprites for examples

        //for each possible position (values 0 to 15)
        //assign a different uv mapping
        u.Add(new Vector2(0.875f, 0.5f));
        u.Add(new Vector2(0.875f, 1f));
        u.Add(new Vector2(1f, 1f));

        u.Add(new Vector2(0.875f, 0.5f));
        u.Add(new Vector2(1f, 1f));
        u.Add(new Vector2(1f, 0.5f));
    }

    /// <summary>
    /// Builds vertice/uv arrays
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="objects"></param>
    /// <param name="r"></param>
    static void ProcessCoordinate(int i, int j, int x, int y, Dictionary<int, MeshObject> objects, Region r,
        ushort value, byte position, TerrainTypes terrain)
    {
        //compute chunk position
        int chunkx = i * TerrainConstants.ChunkSize;
        int chunky = j * TerrainConstants.ChunkSize;

        //Get mesh Object
        MeshObject mObj = default(MeshObject);
        if (!objects.TryGetValue(value, out mObj))
        {
            mObj.vertices = new List<Vector3>();
            mObj.uv_map = new List<Vector2>();
            mObj.triangles = new List<int>();
            mObj.priority = terrain.GetBiomePriority(value);
            mObj.texture_value = value;
            objects.Add(value, mObj);
        }

        //reference names
        List<Vector3> v = mObj.vertices; List<Vector2> u = mObj.uv_map;
        List<int> t = mObj.triangles;

        //count before adding vertices
        int start = v.Count;

        //add vertices
        v.Add(new Vector3(chunkx + x, 0, chunky + y));
        v.Add(new Vector3(chunkx + x, 0, chunky + y + 1));
        v.Add(new Vector3(chunkx + x + 1, 0, chunky + y + 1));

        v.Add(new Vector3(chunkx + x, 0, chunky + y));
        v.Add(new Vector3(chunkx + x + 1, 0, chunky + y + 1));
        v.Add(new Vector3(chunkx + x + 1, 0, chunky + y));

        //assign triangles indices
        t.Add(start); t.Add(start + 1); t.Add(start + 2);
        t.Add(start + 3); t.Add(start + 4); t.Add(start + 5);

        //just for reference: 
        /*
            0  1  2  3  4  5  6  7
            8  9  10 11 12 13 14 15

        */
        //all tilesheet layouts are pictured like above. See Resources/TerrainSprites for examples

        //for each possible position (values 0 to 15)
        //assign a different uv mapping
        if (position == 7)
        {
            u.Add(new Vector2(0.875f, 0.5f));
            u.Add(new Vector2(0.875f, 1f));
            u.Add(new Vector2(1f, 1f));

            u.Add(new Vector2(0.875f, 0.5f));
            u.Add(new Vector2(1f, 1f));
            u.Add(new Vector2(1f, 0.5f));
        }
        else if (position == 4)
        {
            u.Add(new Vector2(0.5f, 0.5f));
            u.Add(new Vector2(0.5f, 1f));
            u.Add(new Vector2(0.625f, 1f));

            u.Add(new Vector2(0.5f, 0.5f));
            u.Add(new Vector2(0.625f, 1f));
            u.Add(new Vector2(0.625f, 0.5f));
        }
        else if (position == 5)
        {
            u.Add(new Vector2(0.625f, 0.5f));
            u.Add(new Vector2(0.625f, 1f));
            u.Add(new Vector2(0.75f, 1f));

            u.Add(new Vector2(0.625f, 0.5f));
            u.Add(new Vector2(0.75f, 1f));
            u.Add(new Vector2(0.75f, 0.5f));
        }
        else if (position == 12)
        {
            u.Add(new Vector2(0.5f, 0f));
            u.Add(new Vector2(0.5f, 0.5f));
            u.Add(new Vector2(0.625f, 0.5f));

            u.Add(new Vector2(0.5f, 0f));
            u.Add(new Vector2(0.625f, 0.5f));
            u.Add(new Vector2(0.625f, 0f));
        }
        else if (position == 13)
        {
            u.Add(new Vector2(0.625f, 0f));
            u.Add(new Vector2(0.625f, 0.5f));
            u.Add(new Vector2(0.75f, 0.5f));

            u.Add(new Vector2(0.625f, 0f));
            u.Add(new Vector2(0.75f, 0.5f));
            u.Add(new Vector2(0.75f, 0f));
        }
        else if (position == 2)
        {
            u.Add(new Vector2(0.25f, 0.5f));
            u.Add(new Vector2(0.25f, 1f));
            u.Add(new Vector2(0.375f, 1f));

            u.Add(new Vector2(0.25f, 0.5f));
            u.Add(new Vector2(0.375f, 1f));
            u.Add(new Vector2(0.375f, 0.5f));
        }
        else if (position == 3)
        {
            u.Add(new Vector2(0.375f, 0.5f));
            u.Add(new Vector2(0.375f, 1f));
            u.Add(new Vector2(0.5f, 1f));

            u.Add(new Vector2(0.375f, 0.5f));
            u.Add(new Vector2(0.5f, 1f));
            u.Add(new Vector2(0.5f, 0.5f));
        }
        else if (position == 10)
        {
            u.Add(new Vector2(0.25f, 0f));
            u.Add(new Vector2(0.25f, 0.5f));
            u.Add(new Vector2(0.375f, 0.5f));

            u.Add(new Vector2(0.25f, 0f));
            u.Add(new Vector2(0.375f, 0.5f));
            u.Add(new Vector2(0.375f, 0f));
        }
        else if (position == 11)
        {
            u.Add(new Vector2(0.375f, 0f));
            u.Add(new Vector2(0.375f, 0.5f));
            u.Add(new Vector2(0.5f, 0.5f));

            u.Add(new Vector2(0.375f, 0f));
            u.Add(new Vector2(0.5f, 0.5f));
            u.Add(new Vector2(0.5f, 0f));
        }
        else if (position == 0)
        {
            u.Add(new Vector2(0f, 0.5f));
            u.Add(new Vector2(0f, 1f));
            u.Add(new Vector2(0.125f, 1f));

            u.Add(new Vector2(0f, 0.5f));
            u.Add(new Vector2(0.125f, 1f));
            u.Add(new Vector2(0.125f, 0.5f));
        }
        else if (position == 1)
        {
            u.Add(new Vector2(0.125f, 0.5f));
            u.Add(new Vector2(0.125f, 1f));
            u.Add(new Vector2(0.25f, 1f));

            u.Add(new Vector2(0.125f, 0.5f));
            u.Add(new Vector2(0.25f, 1f));
            u.Add(new Vector2(0.25f, 0.5f));
        }
        else if (position == 8)
        {
            u.Add(new Vector2(0f, 0f));
            u.Add(new Vector2(0f, 0.5f));
            u.Add(new Vector2(0.125f, 0.5f));

            u.Add(new Vector2(0f, 0f));
            u.Add(new Vector2(0.125f, 0.5f));
            u.Add(new Vector2(0.125f, 0f));
        }
        else if (position == 9)
        {
            u.Add(new Vector2(0.125f, 0f));
            u.Add(new Vector2(0.125f, 0.5f));
            u.Add(new Vector2(0.25f, 0.5f));

            u.Add(new Vector2(0.125f, 0f));
            u.Add(new Vector2(0.25f, 0.5f));
            u.Add(new Vector2(0.25f, 0f));
        }
        //otherwise, unused texture location, error!
        else
        {
            throw (new System.Exception("Cannot handle tile texture position!"));
        }
    }

    /// <summary>
    /// Get a material used for a certain texture
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    static Material GetMaterial(int value, TerrainTypes terrain)
    {
        if (Default == null)
            Default = Resources.Load<Material>("Materials/TerrainShader");
        Material mat = null;
        if (materials.TryGetValue(value, out mat))
        {
            return mat;
        }
        else
        {
            mat = new Material(Default);
            mat.shader = Default.shader;
            mat.mainTexture = terrain.GetTerrainTexture(value);
            materials.Add(value, mat);
            return mat;
        }
    }
    /// <summary>
    /// Get the material for the input terrain object
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="terrain"></param>
    /// <returns></returns>
    static Material GetMaterial(Texture2D texture, TerrainTypes terrain)
    {
        if (Default == null)
            Default = Resources.Load<Material>("Materials/TerrainShader");
        Material mat = null;
        if (terrain_materials.TryGetValue(texture, out mat))
            return mat;
        else
        {
            mat = new Material(Default);
            mat.shader = Default.shader;
            mat.mainTexture = texture;
            terrain_materials.Add(texture, mat);
            return mat;
        }
    }

    /// <summary>
    /// Draws all terrain objects for the input chunk coordinate
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="r"></param>
    /// <param name="p"></param>
    /// <param name="terrain"></param>
    static void DrawTerrainObjects(int i, int j, Region r, Transform p, TerrainTypes terrain)
    {
        Dictionary<Texture2D, MeshTerrainObject> meshObjects = new Dictionary<Texture2D, MeshTerrainObject>();

        Chunk chunk = r.chunks[i, j];

        for (int x = 0; x < TerrainConstants.ChunkSize; x++)
        {
            for (int y = 0; y < TerrainConstants.ChunkSize; y++)
            {
                if (chunk.terrainObjects[x,y] != null)
                {
                    TerrainObject tObj = chunk.terrainObjects[x, y];
                    MeshTerrainObject meshObj = default(MeshTerrainObject);
                    if (!meshObjects.TryGetValue(tObj.texture, out meshObj))
                    {
                        meshObj.texture_value = tObj.texture;
                        meshObj.vertices = new List<Vector3>(); meshObj.triangles = new List<int>(); meshObj.uv_map = new List<Vector2>();
                        meshObjects.Add(meshObj.texture_value, meshObj);
                    }

                    int chunkx = i * TerrainConstants.ChunkSize;
                    int chunky = j * TerrainConstants.ChunkSize;

                    List<Vector3> v = meshObj.vertices; List<Vector2> u = meshObj.uv_map;
                    List<int> t = meshObj.triangles;

                    //count before adding vertices
                    int start = v.Count;

                    //add vertices
                    v.Add(new Vector3(chunkx + x, 0, chunky + y));
                    v.Add(new Vector3(chunkx + x, 0, chunky + y + 1));
                    v.Add(new Vector3(chunkx + x + 1, 0, chunky + y + 1));

                    v.Add(new Vector3(chunkx + x, 0, chunky + y));
                    v.Add(new Vector3(chunkx + x + 1, 0, chunky + y + 1));
                    v.Add(new Vector3(chunkx + x + 1, 0, chunky + y));

                    //assign triangles indices
                    t.Add(start); t.Add(start + 1); t.Add(start + 2);
                    t.Add(start + 3); t.Add(start + 4); t.Add(start + 5);

                    //assign uv positions
                    u.Add(new Vector2(0f, 0f));
                    u.Add(new Vector2(0f, 1f));
                    u.Add(new Vector2(1f, 1f));

                    u.Add(new Vector2(0f, 0f));
                    u.Add(new Vector2(1f, 1f));
                    u.Add(new Vector2(1f, 0f));
                }//endif
            }//endfor
        }//endfor

        //build objects
        foreach (MeshTerrainObject m in meshObjects.Values)
        {
            GameObject newObj = new GameObject(i.ToString() + ", " + j.ToString() +", Obj.");
            MeshRenderer mr = newObj.AddComponent<MeshRenderer>();
            mr.sortingOrder = 1001;
            MeshFilter mf = newObj.AddComponent<MeshFilter>();
            mr.material = GetMaterial(m.texture_value, terrain);
            mf.mesh.vertices = m.vertices.ToArray();
            mf.mesh.uv = m.uv_map.ToArray();
            mf.mesh.triangles = m.triangles.ToArray();
            mf.mesh.Optimize();
            newObj.transform.SetParent(p);
        }
    }
}


public struct MeshObject
{
    public int texture_value;
    public int priority;
    public List<Vector3> vertices;
    public List<Vector2> uv_map;
    public List<int> triangles;
}

public struct MeshTerrainObject
{
    public Texture2D texture_value;
    public List<Vector3> vertices;
    public List<Vector2> uv_map;
    public List<int> triangles;
}