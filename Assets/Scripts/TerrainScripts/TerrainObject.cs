﻿using UnityEngine;
using System.Collections.Generic;


public class TerrainObject : MonoBehaviour
{
    [Tooltip("The name of this object")]
    public string ObjectName;
    [Tooltip("The texture for this object")]
    public Texture2D texture;
    [Tooltip("The width of the patches that this object appears in.")]
    [Range(1, 7)]
    public int size;
    [Tooltip("Chance PER TILE of this object being placed")]
    [Range(0, 1)]
    public float chance;

    [Tooltip("Random size for each patch?")]
    public bool RandomSize = false;
     
    [Tooltip("The approximate density of each patch.")]
    [Range(0,1)]
    public float PatchDensity = 0.5f;
}