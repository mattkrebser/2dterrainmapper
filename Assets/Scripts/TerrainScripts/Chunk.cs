﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// The region is split into many 16x16 chunks. Each chunk is a different peice of land.
/// </summary>
public class Chunk 
{
    /// <summary>
    /// Array holding which biome each x,y position holds
    /// </summary>
    public ushort[,] terrain = new ushort[TerrainConstants.ChunkSize, TerrainConstants.ChunkSize];

    /// <summary>
    /// Terrain objects, like trees and stuff
    /// </summary>
    public TerrainObject[,] terrainObjects = new TerrainObject[TerrainConstants.ChunkSize, TerrainConstants.ChunkSize];
    /*
    terrain_texture_positions:
    for each 8x2 tile, these values
    correspond to which tile is being used 
        
        0  1  2  3  4  5  6  7
        8  9  10 11 12 13 14 15

        terrain_texture_positions[x,y] = 2, means the texture at terrain at (x,y) is using
        the tile in a 8x2 terrain.png at the #2 location

        see resources.terrainsprites for examples
    */

        //thisstructure is an array of hashsets to ensure that only unique edges can be stacked on top of eachother
    /// <summary>
    /// Edge terrains! The boundary textures for each tile set.
    /// </summary>
    public HashSet<terrain_pad_dat>[,] terrain_padding = new HashSet<terrain_pad_dat>[TerrainConstants.ChunkSize, TerrainConstants.ChunkSize];

    /// <summary>
    /// Add a terrain padding object to the terrain padding array
    /// </summary>
    /// <param name="biome"></param>
    /// <param name="position"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void AddTerrainPadding(ushort biome, byte position, int x, int y)
    {
        terrain_pad_dat t = new terrain_pad_dat(biome, position);
        HashSet<terrain_pad_dat> h = terrain_padding[x, y];
        if (h == null)
            terrain_padding[x, y] = h = new HashSet<terrain_pad_dat>();
        if (!h.Contains(t))
            h.Add(t);
    }

    /// <summary>
    /// explictly null all data sets so that they can be collected
    /// </summary>
    public void Clear()
    {
        terrain = null;
        terrain_padding = null;
    }
}

public struct terrain_pad_dat
{
    public ushort biome;
    public byte position;

    public terrain_pad_dat(ushort b, byte p)
    {
        biome = b; position = p;
    }

    public override bool Equals(object obj)
    {
        return obj is terrain_pad_dat ? biome == ((terrain_pad_dat)obj).biome && position == ((terrain_pad_dat)obj).position : false;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = hash * 17 + biome.GetHashCode();
            hash = hash * 17 + biome.GetHashCode();
            return hash;
        }
    }
}
