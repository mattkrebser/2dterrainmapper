﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TerrainTypes : MonoBehaviour
{
    public List<TerrainElement> TerrainList = new List<TerrainElement>();

    /// <summary>
    /// All textures for a terrain
    /// </summary>
    private Texture2D[] terrainTextures;
    [System.NonSerialized]
    private int[] terrainPriorities;
    [System.NonSerialized]
    public ushort[,] BiomeMap;
    [System.NonSerialized]
    public List<ushort[,]> SubBiomeMap;
    [System.NonSerialized]
    public float[] SubBiomeScales; 
    /// <summary>
    /// Number of parent errains
    /// </summary>
    [System.NonSerialized]
    public ushort NumTerrains = 0;
    /// <summary>
    /// List of terrain objects
    /// </summary>
    [System.NonSerialized]
    public List<TerrainObject>[] terrain_objects;
    private bool[] SubBiomeBorderlessOptions;

    void Awake()
    {
        BuildTerrainList();
        if (TerrainList.Count > 1000)
        {
            throw (new System.Exception("Cannot have more than 1000 terrains"));
        }
        NumTerrains = (ushort)TerrainList.Count;
    }

    public int GetBiomePriority(int biomeNumber)
    {
        return terrainPriorities[biomeNumber];
    }
    public Texture2D GetTerrainTexture(int biomeNumber)
    {
        return terrainTextures[biomeNumber];
    }
    /// <summary>
    /// does this map have sub biomes?
    /// </summary>
    /// <returns></returns>
    public bool HasSubBiomes()
    {
        return !(SubBiomeMap == null);
    }
    /// <summary>
    /// does this input biome have sub biomes?
    /// </summary>
    /// <param name="biomeNumber"></param>
    /// <returns></returns>
    public bool HasSubBiomes(int biomeNumber)
    {
        return SubBiomeMap[biomeNumber] != null;
    }
    /// <summary>
    /// Returns true if the input biome is a sub biome
    /// </summary>
    /// <param name="biomeNumber"></param>
    /// <returns></returns>
    public bool IsSubBiome(int biomeNumber)
    {
        return biomeNumber >= TerrainList.Count;
    }
    public bool SubBiomeOverridesParentBorders(int biome)
    {
        return SubBiomeBorderlessOptions[biome];
    }

    void BuildTerrainList()
    {
        if (TerrainList != null && TerrainList.Count > 0)
        {
            //initialize arrays
            int totalBiomes = 0;
            for (int i = 0; i < TerrainList.Count; i++)
            {
                totalBiomes++;
                if (TerrainList[i].SubBiomes != null && TerrainList[i].SubBiomes.Count > 0)
                    totalBiomes += TerrainList[i].SubBiomes.Count;
            }

            if (totalBiomes > 1000)
                throw (new System.Exception("Cannot have more than 1000 different biomes"));

            terrainTextures = new Texture2D[totalBiomes];
            terrainPriorities = new int[totalBiomes];
            terrain_objects = new List<TerrainObject>[totalBiomes];
            SubBiomeBorderlessOptions = new bool[totalBiomes];
            int jSum = 0;
            for (int i = 0; i < TerrainList.Count; i++)
            {
                //assign textures and priority and terrain objects
                terrainTextures[i] = TerrainList[i].terrain;
                terrainPriorities[i] = TerrainList[i].priority;
                terrain_objects[i] = TerrainList[i].TerrainObjects;

                if (terrainPriorities[i] > 1000)
                    throw (new System.Exception("Highest allowed biome priority is 1000!"));

                if (TerrainList[i].TerrainObjects != null && TerrainList[i].TerrainObjects.Count >= 1000)
                    throw (new System.Exception("Cannot have more than 1000 unique objects per biome!"));

                //assign textures and priority and terrain objects for sub biomes as well
                if (TerrainList[i].SubBiomes != null && TerrainList[i].SubBiomes.Count > 0)
                {
                    for (int j = 0; j < TerrainList[i].SubBiomes.Count; j++)
                    {
                        terrainTextures[jSum + TerrainList.Count] = TerrainList[i].SubBiomes[j].terrain;
                        terrainPriorities[jSum + TerrainList.Count] = TerrainList[i].SubBiomes[j].priority;
                        terrain_objects[jSum + TerrainList.Count] = TerrainList[i].SubBiomes[j].TerrainObjects;
                        SubBiomeBorderlessOptions[jSum + TerrainList.Count] = TerrainList[i].SubBiomes[j].OverrideParentBorders;

                        if (terrainPriorities[jSum + TerrainList.Count] > 1000)
                            throw (new System.Exception("Highest allowed biome priority is 1000!"));

                        if (TerrainList[i].SubBiomes[j].TerrainObjects != null && TerrainList[i].SubBiomes[j].TerrainObjects.Count >= 1000)
                            throw (new System.Exception("Cannot have more than 1000 unique objects per biome!"));

                            jSum++;
                    }
                }
            }

            //ensure all biomes have unique draw priorities
            if (!AllPrioritiesAreUnique())
                throw (new System.Exception("Not all biome priorities are unique. All biomes must have unique priorities. This includes sub biomes."));

            //set scales
            SubBiomeScales = new float[TerrainList.Count];
            for (int i = 0; i < TerrainList.Count; i++)
                SubBiomeScales[i] = TerrainList[i].SubBiomesScale;

            //----Start building biome map
            //build prevalnce array
            float[] biomePrevalence = new float[TerrainList.Count];
            for (int i = 0; i < TerrainList.Count; i++)
            {
                if (TerrainList[i].TerrainPrevalence < 0)
                    throw (new System.Exception("Terrain prevalnce values must be greater than or equal to zero."));
                biomePrevalence[i] = TerrainList[i].TerrainPrevalence;
            }
            //get normalized biome prevalence values (Building a PMF that sums to 1024)
            float sum = biomePrevalence.Sum();
            //clamp values inbetween certain ranges.
            if (sum <= 0.00000001f) throw (new System.Exception("Biome Prevalence values are too low for accurate calculation!"));
            if (sum >= 1000000000f) throw (new System.Exception("Biome Prevalence values are too high for accurate calculation!"));
            for (int i = 0; i < TerrainList.Count; i++)
            {
                //normalize this biome to take up x / 1024 tiles
                biomePrevalence[i] = TerrainList[i].TerrainPrevalence / sum * 1024.0f;
            }

            //fill biome map
            FillBiomeMap(biomePrevalence);

            //noise reduction, may not be necessary anymore, but can help in some cases (when there are many biomes (like 100+))
            for (int i = 0; i < 2; i++)
                NoiseReduce();
            //----stop building biome map

            //do subbiome maps
            DoSubBiomeMaps();

        } 
    }

    //description on fill biome map algorithm
    /*
        This fill biome map algorithm fills in a biome array like this:

        starting array: we have to biomes: 1 and 2
        0 0 0
        0 0 0
        0 0 0

        two random start locations are picked
        0 1 0
        0 0 0
        2 0 0

        each biome filled out all of its adjacent neighbors
        1 1 1
        2 1 0
        2 2 0

        each biome filled out its neighbors again, 
        all elements in the array have been used, so done
        1 1 1
        2 1 1
        2 2 2

        the algorithm also takes into account area, which is input as
        a 'biomePrevalence' array.

        In the above array, the 1's have an area of '5'
        and the twos have an area of '4'

        Doing again, but 1's max area = 2, 2's max area = 7
        0 1 0
        0 0 0
        2 0 0

        1 1 0
        2 0 0
        2 2 0

        1 1 0
        2 2 0
        2 2 2

        1 1 0
        2 2 2
        2 2 2

        1 1 2
        2 2 2
        2 2 2
    */

    /// <summary>
    /// Fill out the biomemap table
    /// </summary>
    /// <param name="biomePrevalence"></param>
    /// <param name="biomeNumber"> -1 if filling the main biomes, set to a biome if filling out a subbiomes table</param>
    void FillBiomeMap(float[] biomePrevalence)
    {
        //initialize random point generator
        int[] numbers = new int[1024];
        for (int i = 0; i < numbers.Length; i++) numbers[i] = i;
        RandomSet<int> rand_point = new RandomSet<int>(numbers, 69);

        //initialize points_not_used hashset
        HashSet<int> points_not_used = new HashSet<int>();
        for (int i = 0; i < 1024; i++) points_not_used.Add(i);

        //make new biomemap if biomeNumber = -1
        BiomeMap = new ushort[32, 32];
        //biomes that are currently mapping themselves to the BiomeMap
        //the list<int> represent the current biome tile expansions for each biome
        Stack<int>[] working_biomes = new Stack<int>[TerrainList.Count];

        //corresponds to the number of tiles that a biome has currently filled
        int[] biomeAreas = new int[TerrainList.Count];

        //used for intermmediate calculation
        Stack<int> new_biome_points = new Stack<int>();

        //fill out the biome map until all points are filled
        while (points_not_used.Count > 0)
        {
            //foreach biome
            for (int i = 0; i < working_biomes.Length; i++)
            {
                //add a new list if none
                if (working_biomes[i] == null) working_biomes[i] = new Stack<int>();
                //if this biome is allowed to fill more area...
                if (biomeAreas[i] <= (int)biomePrevalence[i])
                {
                    //if there are tile positions, queued to be added...
                    if (working_biomes[i].Count != 0)
                    {
                        //go through each potential position
                        foreach (int point in working_biomes[i])
                        {
                            //if this point has not already been taken...
                            if (points_not_used.Contains(point))
                            {
                                //add point to biome map
                                int x = point / 32; int y = point % 32;

                                //fill
                                BiomeMap[x, y] = (ushort)i;

                                //increase area for this biome
                                biomeAreas[i]++;
                                //remove point from set of unused points
                                points_not_used.Remove(point);
                                //add adjacent points if they are available
                                if (points_not_used.Contains((x + 1) * 32 + y)) new_biome_points.Push((x + 1) * 32 + y);
                                if (points_not_used.Contains((x - 1) * 32 + y)) new_biome_points.Push((x - 1) * 32 + y);
                                if (points_not_used.Contains(x * 32 + y - 1)) new_biome_points.Push(x * 32 + y - 1);
                                if (points_not_used.Contains(x * 32 + y + 1)) new_biome_points.Push(x * 32 + y + 1);
                            }

                            //stop adding area to this biome if it has enough
                            if (biomeAreas[i] > (int)biomePrevalence[i]) { new_biome_points.Clear(); goto area_filled; };
                        }

                        //if the area has not been filled, add the new points
                        working_biomes[i].Clear();
                        if (new_biome_points.Count != 0)
                            while (new_biome_points.Count > 0) working_biomes[i].Push(new_biome_points.Pop());
                        else
                        {
                            working_biomes[i].Push(rand_point.Next());
                        }

                        area_filled:;
                    }
                    else
                        working_biomes[i].Push(rand_point.Next());
                }
            }
        }
    }

    /// <summary>
    /// Fill out the subbiomemap table
    /// </summary>
    /// <param name="biomePrevalence"></param>
    /// <param name="biomeNumber"> -1 if filling the main biomes, set to a biome if filling out a subbiomes table</param>
    void FillSubBiomeMap(float[] biomePrevalence, int parentBiome, int numSubBiomes, int offset)
    {
        //initialize random point generator
        int[] numbers = new int[1024];
        for (int i = 0; i < numbers.Length; i++) numbers[i] = i;
        RandomSet<int> rand_point = new RandomSet<int>(numbers, 69);

        //initialize points_not_used hashset
        HashSet<int> points_not_used = new HashSet<int>();
        for (int i = 0; i < 1024; i++) points_not_used.Add(i);

        //biomes that are currently mapping themselves to the BiomeMap
        //the list<int> represent the current biome tile expansions for each biome
        Stack<int>[] working_biomes = new Stack<int>[numSubBiomes];

        //corresponds to the number of tiles that a biome has currently filled
        int[] biomeAreas = new int[numSubBiomes];

        //used for intermmediate calculation
        Stack<int> new_biome_points = new Stack<int>();

        //fill out the biome map until all points are filled
        while (points_not_used.Count > 0)
        {
            //foreach biome
            for (int i = 0; i < working_biomes.Length; i++)
            {
                //add a new list if none
                if (working_biomes[i] == null) working_biomes[i] = new Stack<int>();
                //if this biome is allowed to fill more area...
                if (biomeAreas[i] <= (int)biomePrevalence[i])
                {
                    //if there are tile positions, queued to be added...
                    if (working_biomes[i].Count != 0)
                    {
                        //go through each potential position
                        foreach (int point in working_biomes[i])
                        {
                            //if this point has not already been taken...
                            if (points_not_used.Contains(point))
                            {
                                //add point to biome map
                                int x = point / 32; int y = point % 32;

                                //fill, the last element always corresponds to parent biome
                                if (i + 1 >= working_biomes.Length)
                                    SubBiomeMap[parentBiome][x, y] = (ushort)parentBiome;
                                else 
                                    SubBiomeMap[parentBiome][x, y] = (ushort)(i + offset);

                                //increase area for this biome
                                biomeAreas[i]++;
                                //remove point from set of unused points
                                points_not_used.Remove(point);
                                //add adjacent points if they are available
                                if (points_not_used.Contains((x + 1) * 32 + y)) new_biome_points.Push((x + 1) * 32 + y);
                                if (points_not_used.Contains((x - 1) * 32 + y)) new_biome_points.Push((x - 1) * 32 + y);
                                if (points_not_used.Contains(x * 32 + y - 1)) new_biome_points.Push(x * 32 + y - 1);
                                if (points_not_used.Contains(x * 32 + y + 1)) new_biome_points.Push(x * 32 + y + 1);
                            }

                            //stop adding area to this biome if it has enough
                            if (biomeAreas[i] > (int)biomePrevalence[i]) { new_biome_points.Clear(); goto area_filled; };
                        }

                        //if the area has not been filled, add the new points
                        working_biomes[i].Clear();
                        if (new_biome_points.Count != 0)
                            while (new_biome_points.Count > 0) working_biomes[i].Push(new_biome_points.Pop());
                        else
                        {
                            working_biomes[i].Push(rand_point.Next());
                        }

                        area_filled:;
                    }
                    else
                        working_biomes[i].Push(rand_point.Next());
                }
            }
        }
    }

    /// <summary>
    /// Reduce Noise, this is not a smoothing algorithm
    /// </summary>
    void NoiseReduce()
    {
        System.Random noiseRand = new System.Random(69);
        for (int i = 0; i < 32; i++)
        {
            for (int j = 0; j < 32; j++)
            {
                //aquire adjacent biomes, ip means in i+ direction, im means i- direction, jp means j+ direction, jm means j- direction
                //they are set to maxValue so that they will not be equal to the current biome. ushort.maxValue is an impossible value
                ushort ip = ushort.MaxValue, im = ushort.MaxValue, jp = ushort.MaxValue, jm = ushort.MaxValue, b = BiomeMap[i, j];
                if (i + 1 < 32) ip = BiomeMap[i + 1, j]; if (i - 1 > -1) im = BiomeMap[i - 1, j];
                if (j + 1 < 32) jp = BiomeMap[i, j + 1]; if (j - 1 > -1) jm = BiomeMap[i, j - 1];

                //determine number of common adjacent biomes
                int numCommon = 0;
                if (ip == b) numCommon++; if (im == b) numCommon++;
                if (jp == b) numCommon++; if (jm == b) numCommon++;

                //this biome is considered to be noise if it has less than or equal to 1 common neighbor
                if (numCommon <= 1)
                {
                    ushort mostCommonNeighbor = GetMostCommonNeighbor(i, j, ip, im, jp, jm);
                    //if a random value is less than the NoiseReductionCoefficient, then change the biome
                    if (noiseRand.NextDouble() < TerrainList[b].NoiseReductionCoefficient)
                        BiomeMap[i, j] = mostCommonNeighbor;
                }
            }
        }
    }
    /// <summary>
    /// Returns the most common neighbor for the input position and neighbors
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="ip"></param>
    /// <param name="im"></param>
    /// <param name="jp"></param>
    /// <param name="jm"></param>
    /// <returns></returns>
    ushort GetMostCommonNeighbor(int i, int j, ushort ip, ushort im, ushort jp, ushort jm)
    {
        sbyte cip = 0, cim = 0, cjp = 0, cjm = 0;

        //get the number of equal biomes for each adjacent biome
        if (ip == im) cip++;
        if (ip == jp) cip++;
        if (ip == jm) cip++;

        if (im == ip) cim++;
        if (im == jp) cim++;
        if (im == jm) cim++;

        if (jp == jm) cjp++;
        if (jp == ip) cjp++;
        if (jp == im) cjp++;

        if (jm == jp) cjm++;
        if (jm == ip) cjm++;
        if (jm == im) cjm++;

        //since we set border tiles(ip,im,jp,jm) equal to ushort.maxvalue, they cannot be considered in the calculation for most common neighbor
        //these if statements remove border (non existing) biomes from the calculation
        if (ip == ushort.MaxValue) cip = -1; if (im == ushort.MaxValue) cim = -1;
        if (jp == ushort.MaxValue) cjp = -1; if (jm == ushort.MaxValue) cjm = -1;

        //determine which adjenct biome is the most common and return it
        if (cip > cim)
        {
            if (cip > cjp)
            {
                if (cip > cjm)
                    return (ushort)ip;
                else
                    return (ushort)jm;
            }
            else
            {
                if (cjp > cjm)
                    return (ushort)jp;
                else
                    return (ushort)jm;
            }
        }
        else
        {
            if (cim > cjp)
            {
                if (cim > cjm)
                    return (ushort)im;
                else
                    return (ushort)jm;
            }
            else
            {
                if (cjp > cjm)
                    return (ushort)jp;
                else
                    return (ushort)jm;
            }
        }
    }

    /// <summary>
    /// Fill out sub biome map tables
    /// </summary>
    void DoSubBiomeMaps()
    {
        //initialize table
        SubBiomeMap = new List<ushort[,]>();
        for (int i = 0; i < TerrainList.Count; i++)
        {
            SubBiomeMap.Add(null);
            if (TerrainList[i].SubBiomes != null && TerrainList[i].SubBiomes.Count > 0)
                SubBiomeMap[i] = new ushort[32, 32];
        }

        //fill table for each sub biome
        for (int i = 0; i < TerrainList.Count; i++)
        {
            //if sub biomes
            if (TerrainList[i].SubBiomes != null && TerrainList[i].SubBiomes.Count > 0)
            {
                //+ 1 in length because we add the parent biome at the end
                float[] biomePrevalence = new float[TerrainList[i].SubBiomes.Count + 1];
                for (int j = 0; j < TerrainList[i].SubBiomes.Count; j++)
                {
                    if (TerrainList[i].SubBiomes[j].TerrainPrevalence < 0)
                        throw (new System.Exception("Terrain prevalnce values must be greater than or equal to zero."));
                    biomePrevalence[j] = TerrainList[i].SubBiomes[j].TerrainPrevalence;
                }

                //set the parent element
                biomePrevalence[biomePrevalence.Length - 1] = TerrainList[i].SubBiomePrevalence;

                //get normalized biome prevalence values (Building a PMF that sums to 1024)
                float sum = biomePrevalence.Sum();
                //clamp values inbetween certain ranges.
                if (sum <= 0.00000001f) throw (new System.Exception("SubBiome Prevalence values in:"+TerrainList[i].TerrainName+" are too low for accurate calculation!"));
                if (sum >= 1000000000f) throw (new System.Exception("SubBiome Prevalence values in:" + TerrainList[i].TerrainName + "are too high for accurate calculation!"));
                for (int j = 0; j < TerrainList[i].SubBiomes.Count; j++)
                {
                    //normalize this biome to take up x / 1024 tiles
                    biomePrevalence[j] = TerrainList[i].SubBiomes[j].TerrainPrevalence / sum * 1024.0f;
                }
                biomePrevalence[biomePrevalence.Length - 1] = TerrainList[i].SubBiomePrevalence / sum * 1024.0f;

                //find where this biome starts in the priority array, this just
                //matches it with its biome index
                int offset = 0;
                for (int n = 0; n < terrainPriorities.Length; n++)
                {
                    if (terrainPriorities[n] == TerrainList[i].SubBiomes[0].priority) offset = n;
                }

                FillSubBiomeMap(biomePrevalence, i, TerrainList[i].SubBiomes.Count + 1, offset);
            }
        }
    }

    /// <summary>
    /// Returns true if all biomes and sub biomes contain unique priorities
    /// </summary>
    /// <returns></returns>
    bool AllPrioritiesAreUnique()
    {
        HashSet<int> priorities = new HashSet<int>();
        for (int i = 0; i < TerrainList.Count; i++)
        {
            //see if biome priority already exists
            int val = TerrainList[i].priority;
            if (priorities.Contains(val))
                return false;
            priorities.Add(val);

            //check sub biomes
            if (TerrainList[i].SubBiomes != null && TerrainList[i].SubBiomes.Count > 0)
            {
                for (int j = 0; j < TerrainList[i].SubBiomes.Count; j++)
                {
                    int subval = TerrainList[i].SubBiomes[j].priority;
                    if (priorities.Contains(subval))
                        return false;
                    priorities.Add(subval);
                }
            }
        }
        return true;
    }
}

[System.Serializable]
public class TerrainElement
{
    public string TerrainName;
    /// <summary>
    /// Name of this terrain
    /// </summary>
    [Tooltip("Terrain texture")]
    public Texture2D terrain;

    [Range(0, 2000)]
    /// <summary>
    /// Higher means that this texture will be drawn on top
    /// </summary>
    [Tooltip("Higher means that this texture will be drawn on top")]
    public int priority;

    [Range(0, 1000)]
    [Tooltip("Higher than the other terrains makes this terrain more common. Lower than the other terrains makes this terrain less common.")]
    /// <summary>
    /// How common is this terrain? If this number is comparitivly higher than the other terrain's 'TerrainPrevelance', then this terrain will be more common.
    /// A value of '0' means none.
    /// </summary>
    public float TerrainPrevalence = 1f;

    [Range(0, 1)]
    [Tooltip("If this terrain is determined to be a noise value of the sorrounding terrain, then there is a 'NoiseReductionCoefficient' percent chance that it will be removed")]
    /// <summary>
    /// If this terrain is determined to be a noise value of the sorrounding terrain, then there is a
    /// 'NoiseReductionCoefficient' percent chance that it will be removed
    /// </summary>
    public float NoiseReductionCoefficient = 0.5f;

    [Range(0, 300)]
    [Tooltip("Terrain Scale of SUb Biomes. Large will make greater chunks of sub biomes, smaller will make more noisy sub biomes features")]
    /// <summary>
    /// Terrain Scale value of Sub Biomes
    /// </summary>
    public float SubBiomesScale = 1.0f;

    [Range(0, 1000)]
    [Tooltip("SubBiome prevalence. How prevalent is this biome compared to its sub biomes?")]
    /// <summary>
    /// How prevalent is this biome compared to its subbiomes?
    /// </summary>
    public float SubBiomePrevalence = 1.0f;

    public List<SubTerrain> SubBiomes;
    public List<TerrainObject> TerrainObjects;
}

[System.Serializable]
public class SubTerrain
{
    public string TerrainName;

    /// <summary>
    /// Name of this terrain
    /// </summary>
    [Tooltip("Terrain texture")]
    public Texture2D terrain;

    /// <summary>
    /// Higher means that this texture will be drawn on top
    /// </summary>
    [Range(0, 2000)]
    [Tooltip("Higher means that this texture will be drawn on top")]
    public int priority;

    /// <summary>
    /// How common is this terrain? If this number is comparitivly higher than the other terrain's 'TerrainPrevelance', then this terrain will be more common.
    /// A value of '0' means none.
    /// </summary>
    [Range(0, 1000)]
    [Tooltip("Higher than the other terrains makes this terrain more common. Lower than the other terrains makes this terrain less common.")]
    public float TerrainPrevalence = 1f;

    public List<TerrainObject> TerrainObjects;

    /// <summary>
    /// If true, this biome will allow itself to touch parent biomes that aren't its own parent.
    /// If false, a beach-like effect is created
    /// </summary>
    [Tooltip("If true, this biome will allow itself to touch parent biomes that aren't its own parent.\n If false, a beach-like effect is created")]
    public bool OverrideParentBorders = false;
}