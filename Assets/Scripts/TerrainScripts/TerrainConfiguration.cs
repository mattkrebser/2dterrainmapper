﻿using UnityEngine;

[System.Serializable]
public class TerrainConfiguration
{
    /// <summary>
    /// seed to generate map
    /// </summary>
    public long seed;
    /// <summary>
    /// Size of the map in chunks
    /// </summary>
    [Tooltip("Map size in chunks")]
    [Range(0, 100)]
    public int MapSize;

    [Range(0, 300)]
    /// <summary>
    /// Size of the terrain elements
    /// </summary>
    public float TerrainScale = 1.0f;
    /// <summary>
    /// More square-like worlds
    /// </summary>
    public bool SquareGeneration = false;

    public bool useSecondaryNoise = false;

    [Range(0, 1)]
    /// <summary>
    /// Extra noise for more interesting terrain
    /// </summary>
    public float SecondaryNoiseSize = 0.30f;

    public bool useThirdNoise = false;
    [Range(0,1)]
    /// <summary>
    /// Extra noise for more interesting terrain
    /// </summary>
    public float ThirdNoiseSize = 0.15f;
}
