﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// A random item generator that does not repeat elements
/// </summary>
/// <typeparam name="T"></typeparam>
public class RandomSet<T>
{

    private T[] set;
    int inc = -1;

    public RandomSet(T[] items, int seed)
    {
        //initialize
        System.Random r = new System.Random(seed);
        set = new T[items.Length];
        //copy
        for (int i = 0; i < items.Length; i++)
            set[i] = items[i];
        //shuffle
        for (int i = 0; i < set.Length; i++)
        {
            int p = r.Next(0, set.Length);
            T v = set[i];
            set[i] = set[p];
            set[p] = v;
        }
    }

    /// <summary>
    /// Get next random item from this set.
    /// </summary>
    /// <returns></returns>
    public T Next()
    {
        if (set == null) throw new System.Exception("RandomSet is null, cannot return.");
        inc = inc + 1 >= set.Length ? 0 : inc + 1;
        return set[inc];
    }

    /// <summary>
    /// Returns true if this generator has gone through all items in the set
    /// </summary>
    public bool ExhaustedSet
    {
        get
        {
            if (set == null) throw new System.Exception("RandomSet is null, cannot return.");
            return inc >= set.Length;
        }
    }
}
