﻿using UnityEngine;
using System.Collections;

public class TerrainConstants
{
    /// <summary>
    /// Area of chunks = ChunkSizexChunkSize
    /// </summary>
    public static int ChunkSize = 50;
}