﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Contains all terrain data for whatever map is being played
/// </summary>
public class Region : MonoBehaviour
{
    /// <summary>
    /// All chunks in the region.
    /// </summary>
    public Chunk[,] chunks;
    /// <summary>
    /// The size of the chunks array, worldsize*worldsize. The chunks array is always a square
    /// </summary>
    public int world_size;


    /// <summary>
    /// specific use. Used to determine if an in bounds coordinate with the exception that
    /// the 'x' parameter has + 1 added to it, is inside of the world.
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public bool InBoundsXPlus(int i, int x)
    {
        if (x < TerrainConstants.ChunkSize)
            return true;
        if (i + 1 >= world_size)
            return false;
        return true;
    }
    public bool InBoundsXMinus(int i, int x)
    {
        if (x >= 0) return true;
        if (i - 1 < 0) return false;
        return true;
    }
    public bool InBoundsYPlus(int j, int y)
    {
        if (y < TerrainConstants.ChunkSize)
            return true;
        if (j + 1 >= world_size)
            return false;
        return true;
    }
    public bool InBoundsYMinus(int j, int y)
    {
        if (y >= 0) return true;
        if (j - 1 < 0) return false;
        return true;
    }


    /// <summary>
    /// Use only in-world coordinates. Can only handle 
    /// x/y values that are less than TerrainConstants.ChunkSize out of bounds.
    /// Example usage: someBiome = TileAt(i, j, x + 4, y - 4)
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    public ushort TileAt(int i, int j, int x, int y)
    {
        if (x < 0)
        {
            i -= 1; x = TerrainConstants.ChunkSize - 1;
        }
        if (x >= TerrainConstants.ChunkSize)
        {
            i += 1; x = 0;
        }
        if (y < 0)
        {
            j -= 1; y = TerrainConstants.ChunkSize - 1;
        }
        if (y >= TerrainConstants.ChunkSize)
        {
            j += 1; y = 0;
        }

        if (i > -1 && i < world_size && j > -1 && j < world_size)
            return chunks[i, j].terrain[x, y];
        throw (new System.Exception("Tried to access out of bounds coordinate"));
    }

    /// <summary>
    /// Outs the terrain value at the input coordinates. Returns false if failed.
    /// Can only handle blocks that are +- 1 away
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val"></param>
    /// <returns></returns>
    public bool TryGetTileAt(int i, int j, int x, int y, out ushort val)
    {
        if (x < 0)
        {
            i -= 1; x = TerrainConstants.ChunkSize - 1;
        }
        if (x >= TerrainConstants.ChunkSize)
        {
            i += 1; x = 0;
        }
        if (y < 0)
        {
            j -= 1; y = TerrainConstants.ChunkSize - 1;
        }
        if (y >= TerrainConstants.ChunkSize)
        {
            j += 1; y = 0;
        }

        if (i > -1 && i < world_size && j > -1 && j < world_size)
        {
            val = chunks[i, j].terrain[x, y]; return true;
        }
        val = 0; return false;
    }

    /// <summary>
    /// Attempts to add chunk padding. Does not notify if failed.
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="biome"></param>
    /// <param name="position"></param>
    public void TryAddChunkPadding(int i, int j, int x, int y, ushort biome, byte position)
    {
        if (x < 0)
        {
            i -= 1; x = TerrainConstants.ChunkSize - 1;
        }
        if (x >= TerrainConstants.ChunkSize)
        {
            i += 1; x = 0;
        }
        if (y < 0)
        {
            j -= 1; y = TerrainConstants.ChunkSize - 1;
        }
        if (y >= TerrainConstants.ChunkSize)
        {
            j += 1; y = 0;
        }

        if (i > -1 && i < world_size && j > -1 && j < world_size)
        {
            chunks[i, j].AddTerrainPadding(biome, position, x, y);
        }
    }
    /// <summary>
    /// Use only in-world coordinates. Can only handle 
    /// x/y values that are less than TerrainConstants.ChunkSize out of bounds. Does not throw exceptions.
    /// Example usage: SetTileAt(i, j, x + 4, y - 4, 3) -> sets biome to 3
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public void TrySetTileAt(int i, int j, int x, int y, ushort value)
    {
        if (x < 0)
        {
            i -= 1; x = TerrainConstants.ChunkSize - 1;
        }
        if (x >= TerrainConstants.ChunkSize)
        {
            i += 1; x = 0;
        }
        if (y < 0)
        {
            j -= 1; y = TerrainConstants.ChunkSize - 1;
        }
        if (y >= TerrainConstants.ChunkSize)
        {
            j += 1; y = 0;
        }

        if (i > -1 && i < world_size && j > -1 && j < world_size)
        {
            chunks[i, j].terrain[x, y] = value; return;
        }
    }
    /// <summary>
    /// Try to set a terrain object at the input coordinates, returns false if failed
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public bool TrySetFarTerrainObjectAt(int i, int j, int x, int y, TerrainObject value)
    {
        if (x < 0)
        {
            i -= 1; x += TerrainConstants.ChunkSize;
        }
        if (x >= TerrainConstants.ChunkSize)
        {
            i += 1; x -= TerrainConstants.ChunkSize;
        }
        if (y < 0)
        {
            j -= 1; y += TerrainConstants.ChunkSize;
        }
        if (y >= TerrainConstants.ChunkSize)
        {
            j += 1; y -= TerrainConstants.ChunkSize;
        }

        if (i > -1 && i < world_size && j > -1 && j < world_size)
        {
            chunks[i, j].terrainObjects[x, y] = value; return true;
        }

        return false;
    }

    /// <summary>
    /// Returns true if there is a higher priority biome adjacent to the input biome
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="biome"></param>
    /// <returns></returns>
    public bool HasHighPriorityNeighbor(int i, int j, int x, int y, ushort biome, TerrainTypes terrain)
    {
        ushort xp = 0, xm = 0, yp = 0, ym = 0;
        ushort xpyp = 0, xmym = 0, ypxm = 0, ymxp = 0;

        if (TryGetFarTileAt(i, j, x + 1, y, out xp))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(xp)) return true;
        if (TryGetFarTileAt(i, j, x - 1, y, out xm))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(xm)) return true;
        if (TryGetFarTileAt(i, j, x, y + 1, out yp))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(yp)) return true;
        if (TryGetFarTileAt(i, j, x, y - 1, out ym))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(ym)) return true;

        if (TryGetFarTileAt(i, j, x + 1, y + 1, out xpyp))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(xpyp)) return true;
        if (TryGetFarTileAt(i, j, x - 1, y - 1, out xmym))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(xmym)) return true;
        if (TryGetFarTileAt(i, j, x - 1, y + 1, out ypxm))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(ypxm)) return true;
        if (TryGetFarTileAt(i, j, x + 1, y - 1, out ymxp))
            if (terrain.GetBiomePriority(biome) < terrain.GetBiomePriority(ymxp)) return true;

        return false;
    }

    /// <summary>
    /// Returns true if the input position has any neighboring parent biomes
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="biome"></param>
    /// <param name="exempt"></param>
    /// <returns></returns>
    public bool HasAnyParentBiomeNeighbor(int i ,int j, int x, int y, ushort biome, TerrainTypes terrain)
    {
        ushort xp = 0, xm = 0, yp = 0, ym = 0;
        ushort xpyp = 0, xmym = 0, ypxm = 0, ymxp = 0;

        if (TryGetTileAt(i, j, x + 1, y, out xp))
            if (xp != biome && !terrain.IsSubBiome(xp)) return true;
        if (TryGetTileAt(i, j, x - 1, y, out xm))
            if (xm != biome && !terrain.IsSubBiome(xm)) return true;
        if (TryGetTileAt(i, j, x, y + 1, out yp))
            if (yp != biome && !terrain.IsSubBiome(yp)) return true;
        if (TryGetTileAt(i, j, x, y - 1, out ym))
            if (ym != biome && !terrain.IsSubBiome(ym)) return true;

        if (TryGetTileAt(i, j, x + 1, y + 1, out xpyp))
            if (xpyp != biome && !terrain.IsSubBiome(xpyp)) return true;
        if (TryGetTileAt(i, j, x - 1, y - 1, out xmym))
            if (xmym != biome && !terrain.IsSubBiome(xmym)) return true;
        if (TryGetTileAt(i, j, x - 1, y + 1, out ypxm))
            if (ypxm != biome && !terrain.IsSubBiome(ypxm)) return true;
        if (TryGetTileAt(i, j, x + 1, y - 1, out ymxp))
            if (ymxp != biome && !terrain.IsSubBiome(ymxp)) return true;

        return false;
    }

    /// <summary>
    /// Outs a terrain value at most one chunk away
    /// </summary>
    /// <param name="i"></param>
    /// <param name="j"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="val"></param>
    /// <returns></returns>
    public bool TryGetFarTileAt(int i, int j, int x, int y, out ushort val)
    {
        if (x < 0)
        {
            i -= 1; x += TerrainConstants.ChunkSize;
        }
        else if (x >= TerrainConstants.ChunkSize)
        {
            i += 1; x -= TerrainConstants.ChunkSize;
        }
        if (y < 0)
        {
            j -= 1; y += TerrainConstants.ChunkSize;
        }
        else if (y >= TerrainConstants.ChunkSize)
        {
            j += 1; y -= TerrainConstants.ChunkSize;
        }

        if (i > -1 && i < world_size && j > -1 && j < world_size)
        {
            val = chunks[i, j].terrain[x, y]; return true;
        }
        val = 0; return false;
    }
}

